import {Text, TouchableOpacity, StyleSheet} from "react-native";

type TodoItemProps = {
    item: {text:string, key:string},
    pressHandler: (key:string) => void;
}
export default function TodoItem(props: TodoItemProps){
    return (
        <TouchableOpacity onPress={()=>props.pressHandler(props.item.key)}>
             <Text style={styles.item}>{props.item.text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    item: {
        padding: 16,
        marginTop: 16,
        borderColor: '#bbb',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: 10,
        flexDirection: 'row',
    },
    itemText: {
        marginLeft: 10
    }
})